package com.example.arzhang.exercise.Utils;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class BaseApplication extends Application {

    static BaseApplication baseApp;

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        baseApp=this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
