package com.example.arzhang.exercise.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class Publics {


    public static void toast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

//    public static Typeface getTypeFace(){
//        return Typeface.createFromAsset(
//                MyApplication.appContext.getAssets() , "sans.ttf"
//        ) ;
//    }

    public static void setShared(String key, String value) {
        Hawk.put(key, value);
//        PreferenceManager.getDefaultSharedPreferences(mContext).
//                edit().putString(key, value).apply();
    }

    public static String getShared(String key, String defValue) {
        return Hawk.get(key, defValue);
//        return PreferenceManager.getDefaultSharedPreferences(mContext)
//                .getString(key, defValue);
    }


}
