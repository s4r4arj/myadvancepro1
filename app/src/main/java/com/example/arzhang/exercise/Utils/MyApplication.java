package com.example.arzhang.exercise.Utils;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

public class MyApplication extends Application {

    public static Context appContext;

    public MyApplication() {
    }

    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        appContext = this;
    }

}
