package com.example.arzhang.exercise;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.bumptech.glide.Glide;
import com.example.arzhang.exercise.Utils.BaseActivity;
import com.example.arzhang.exercise.Utils.Publics;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class Tamrin1 extends BaseActivity  {
    RoundedImageView img;
    EditText name;
    EditText code;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        getPermission();
        bind();

    }

    private void bind() {
        img=findViewById( R.id.img );
        name=findViewById( R.id.name );
        code=findViewById( R.id.code );
        email=findViewById( R.id.email );
        findViewById( R.id.openDialog ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        } );
        findViewById( R.id.save ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameV = name.getText().toString();
                String codeV = code.getText().toString();
                String emailV = email.getText().toString();

                Publics.setShared("name", nameV);
                Publics.setShared("code", codeV);
                Publics.setShared("email", emailV);

                name.setText("");
                code.setText("");
                email.setText("");

                Publics.toast(mContext, "thanks");

            }
        } );

    }


    private void openDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(mContext).create();
        dialog.setTitle("Selection");

        dialog.setMessage("Please Select One Of These :");
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "camera"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        EasyImage.openCamera(Tamrin1.this , 0);
//                        dialog.dismiss();
//                        Toast.makeText(mContext, "on click at btn", Toast.LENGTH_SHORT).show();
                    }
                });
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "gallery"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EasyImage.openGallery(Tamrin1.this , 1);
//                        dialog.dismiss();
//                        Toast.makeText(mContext, "on click at btn", Toast.LENGTH_SHORT).show();
                    }
                });

        dialog.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                switch(type) {
                    case 0:
                        Glide.with( Tamrin1.this )
                                .load( imageFile ).
                                into( img );
                    case 1:
                        Glide.with( Tamrin1.this )
                                .load( imageFile ).
                                into( img );

                }
            }
        });


    }



    void getPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }
}
