package com.example.arzhang.exercise.CustomView;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

public class MyImageView extends AppCompatImageView {
    Context mContext;
    public MyImageView(Context context) {
        super( context );
        this.mContext=context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super( context, attrs );
        this.mContext = context;
    }


    public void Load(String url){

        Glide.with( this.mContext ).load( url ).into( this );

    }
}